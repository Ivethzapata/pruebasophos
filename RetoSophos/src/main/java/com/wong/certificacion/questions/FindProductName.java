package com.wong.certificacion.questions;


import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class FindProductName implements Question<String>{
	
	public static FindProductName onCar() {
		return new FindProductName();
	}


	@Override
	public String answeredBy(Actor actor) {
		
		return  Text.of(CyberWongPage.BTN_PRODUCTITEM).viewedBy(actor).asString();
	}

}
