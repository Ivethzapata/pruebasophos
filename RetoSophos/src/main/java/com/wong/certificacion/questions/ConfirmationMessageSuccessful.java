package com.wong.certificacion.questions;


import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


public class ConfirmationMessageSuccessful implements Question<String> {
	

    
    public static ConfirmationMessageSuccessful onPage() {
        return new ConfirmationMessageSuccessful();
    }

	@Override
	public String answeredBy(Actor actor) {
	        
	        return  Text.of(CyberWongPage.MESSAGE).viewedBy(actor).asString();
	   
	}
}



