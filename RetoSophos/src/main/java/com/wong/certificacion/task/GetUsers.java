package com.wong.certificacion.task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;

public class GetUsers implements Task {
    private String page;

    public GetUsers(String page) {
        this.page = page;
    }

    public static GetUsers fromPage(String page) {
        return Tasks.instrumented(GetUsers.class, page);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Get.resource(page));
    }
}
