package com.wong.certificacion.task;

import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class SearchProduct implements Task {

	private String productName;

	public SearchProduct(String productName) {
		this.productName = productName;
	}

	public static SearchProduct onPage(String productName) {

		return Tasks.instrumented(SearchProduct.class, productName);

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Enter.theValue(productName).into(CyberWongPage.TXT_SEARCHPRODUCT));
		actor.attemptsTo(WaitUntil.the(CyberWongPage.LKN_VERRESULTADO, WebElementStateMatchers.isVisible()));
		actor.attemptsTo(Click.on(CyberWongPage.LKN_VERRESULTADO));

	}

}
