package com.wong.certificacion.task;




import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;

public class DeclinesNotifications implements Task {
	
	public static DeclinesNotifications onPage() {
	return Tasks.instrumented(DeclinesNotifications.class);
	
	

}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(WaitUntil.the(CyberWongPage.BTN_CANCELNOTIFICATION,WebElementStateMatchers.isVisible()));
		actor.attemptsTo(Click.on(CyberWongPage.BTN_CANCELNOTIFICATION));
		
	}
	
}
