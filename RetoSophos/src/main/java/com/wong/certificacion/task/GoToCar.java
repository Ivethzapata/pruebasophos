package com.wong.certificacion.task;

import com.wong.certificacion.interactions.Wait;
import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;

import net.serenitybdd.screenplay.waits.WaitUntil;

public class GoToCar implements Task {
	
	public static GoToCar onPage() {
		return Tasks.instrumented(GoToCar.class);
		
		
	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Click.on(CyberWongPage.BTN_CLOSNOTIFICATION));
		actor.attemptsTo(WaitUntil.the(CyberWongPage.BTN_CLOSNOTIFICATION, WebElementStateMatchers.isClickable()));
		actor.attemptsTo(Wait.aMoment(2));
		actor.attemptsTo(Click.on(CyberWongPage.BTN_MINICAR), Click.on(CyberWongPage.BTN_VERCARRITO));
		
		actor.attemptsTo(WaitUntil.the(CyberWongPage.BTN_PRODUCTITEM, WebElementStateMatchers.isVisible()));
	}

}
