package com.wong.certificacion.task;

import static com.wong.certificacion.userinterface.Constant.PRODUCT_NAME_PAGE;


import com.wong.certificacion.userinterface.CyberWongPage;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;

public class BuyProduct implements Task {
	
	public static BuyProduct onPage() {
		return Tasks.instrumented(BuyProduct.class);

	}

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.remember(PRODUCT_NAME_PAGE,BrowseTheWeb.as(actor).getDriver().findElement(By.xpath("//a[@class='product-item__name']")).getText());
		actor.attemptsTo(Click.on(CyberWongPage.BTN_COMPRAR));
		
	}
	
	

}
