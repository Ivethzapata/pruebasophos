package com.wong.certificacion.interactions;

import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;



import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;

public class Wait implements Interaction{

	private int tiempo;

    public Wait(int tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        Awaitility.await().forever().pollInterval(tiempo, TimeUnit.SECONDS).until(() -> true);

    }

    public static Wait aMoment(int tiempo) {
        return Tasks.instrumented(Wait.class, tiempo);
    }
}
