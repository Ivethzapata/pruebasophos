package com.wong.certificacion.userinterface;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class CyberWongPage extends PageObject {
	
 public static  final Target TXT_SEARCHPRODUCT = Target.the("Input pruduct name").located(By.id("search-autocomplete-input"));
 public static  final Target LKN_VERRESULTADO = Target.the("Link ver todos los  resultado").locatedBy("//a[@class='see-all']");
 public static  final Target  DIV_LOADER = Target.the("Cargando busqueda").locatedBy("//div[@class = 'loader' and @style='display: block;']");
 public static  final Target  BTN_COMPRAR = Target.the("Boton compra").locatedBy("//button [@ class ='product-item__add-to-cart product-add-to-cart btn red add-to-cart']");
 public static  final Target BTN_CANCELNOTIFICATION = Target.the("Cancelar la notificación de alerta").located(By.id("onesignal-popover-cancel-button"));
 public static  final Target  BTN_CLOSNOTIFICATION = Target.the("Cerrar notificacion").locatedBy("//button[@class='modal-address__close font-icn cross']");
 public static  final Target  MESSAGE = Target.the("Mensaje ¡Producto agregado al carrito!").locatedBy("//div[@class='shipping-preference__title delivery__login-title']");
 public static  final Target  BTN_MINICAR = Target.the("Carrito de compras").locatedBy("//button[@class='btn red minicart__action--toggle-open food-site']");
 public static  final Target  BTN_VERCARRITO = Target.the("Ver Carrito").locatedBy("//a[contains(@class='minicart__action minicart__action--buy') and @style='display: flex;']");
 public static  final Target  BTN_PRODUCTITEM = Target.the("Product Item").locatedBy("//div[@class='product-item__info']");


 
	
	
	
	

}
