package com.wong.certificacion.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        plugin = {"pretty"},
        features = "src/test/resources/features/BuyPruduct.feature",
        glue = {"com.wong.certificacion.stepdefinitions"},
        snippets = SnippetType.CAMELCASE)
public class BuyPruductRunners {

}
