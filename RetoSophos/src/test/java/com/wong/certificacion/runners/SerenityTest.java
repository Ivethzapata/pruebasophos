package com.wong.certificacion.runners;


import net.serenitybdd.junit.runners.SerenityRunner;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.wong.certificacion.models.*;
import com.wong.certificacion.questions.GetUserInPage;
import com.wong.certificacion.questions.ResponseCode;
import com.wong.certificacion.task.GetUsers;
import com.wong.certificacion.task.RegisterUser;

import static org.hamcrest.Matchers.*;

@RunWith(SerenityRunner.class)
public class SerenityTest {
    private static final String URl="https://gorest.co.in/";
    @Test
    public void getUser(){
        Actor Estefa=Actor.named("Nia").whoCan(CallAnApi.at(URl));
        Estefa.attemptsTo(GetUsers.fromPage("public-api/users?access-token=QX2g1Sh_CXhHJXvObqQeMxbLyFoJ6OpA1zTg&last_name=Bernal"));
        Estefa.should(GivenWhenThen.seeThat("The code is",ResponseCode.onPage(),equalTo(200)));
        Result dato= new GetUserInPage().answeredBy(Estefa).getResult().stream().filter(x ->x.getId()=="9230").findFirst().orElse(null);
        Estefa.should(GivenWhenThen.seeThat("The user no found",act->dato,notNullValue()));
        Estefa.should(GivenWhenThen.seeThat("The user email is: ",act->dato.getEmail(),equalTo("fulanito2@example.com")));
    }
    @Test
    public void registerUserTestModel(){
        Actor Estefa=Actor.named("Nia").whoCan(CallAnApi.at(URl));
        RegisterUserModels user= new RegisterUserModels();
        user.setFirstName("Jose");
        user.setLastName("Perez");
        user.setGender("male");
        user.setDob("1960-07-05");
        user.setEmail("marianasalazar50@example.com");
        user.setPhone("3124321234");
        user.setWebsite("http://www.streich.com/assumenda-molestiae-qui-enim-velit");
        user.setAddress("5922 Cummerata Islands");
        user.setStatus("active");
        Estefa.attemptsTo(RegisterUser.withInfo(user));
        Estefa.should(GivenWhenThen.seeThat("The code is",ResponseCode.onPage(),equalTo(200)));
    }
}
