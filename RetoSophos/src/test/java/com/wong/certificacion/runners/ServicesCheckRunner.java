package com.wong.certificacion.runners;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
		plugin = {"pretty"},
        features = "src/test/resources/features/ServiceCheck.feature",
        glue = {"com.wong.certificacion.stepdefinitions"},
        snippets = SnippetType.CAMELCASE)
public class ServicesCheckRunner {

}

