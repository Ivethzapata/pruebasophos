package com.wong.certificacion.stepdefinitions;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.rest.abiities.CallAnApi;
import static org.hamcrest.Matchers.equalTo;

import com.wong.certificacion.questions.ResponseCode;
import com.wong.certificacion.task.RegisterUser;

public class RegisterUserStepDefinitons {
    private static final String URl="https://gorest.co.in/";
    Actor Estefa=Actor.named("Nia").whoCan(CallAnApi.at(URl));

    @Then("^a user is created with the name (.*)$")
    public void aUserIsCreatedWithTheNameMariana(String nombre) {
        String info="{\n" +
                "    \"first_name\": \""+nombre +"\",\n" +
                "    \"last_name\": \"Salazar\",\n" +
                "    \"gender\": \"male\",\n" +
                "   \"dob\": \"1960-07-05\",\n" +
                "   \"email\": \"salomesalazar50@gmail.com\",\n" +
                "            \"phone\": \"1-552-785-9448 x85213\",\n" +
                "            \"website\": \"http://www.streich.com/assumenda-molestiae-qui-enim-velit\",\n" +
                "            \"address\": \"5922 Cummerata Islands nVivienneborough, NE 46591\",\n" +
                "            \"status\": \"active\"\n" +
                "}";
        Estefa.attemptsTo(RegisterUser.withInfo(info));
    }

    @When("^it is validated that the server responded correctly with code (.*)$")
    public void itIsValidatedThatTheServerRespondedCorrectlyWithCode(int code) {
        Estefa.should(GivenWhenThen.seeThat("The code is", ResponseCode.onPage(),equalTo(code)));
    }


}
