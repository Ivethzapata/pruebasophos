package com.wong.certificacion.stepdefinitions;


import static com.wong.certificacion.userinterface.Constant.PRODUCT_NAME_PAGE;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

import org.hamcrest.Matchers;

import com.wong.certificacion.questions.ConfirmationMessageSuccessful;
import com.wong.certificacion.questions.FindProductName;
import com.wong.certificacion.task.BuyProduct;
import com.wong.certificacion.task.DeclinesNotifications;
import com.wong.certificacion.task.GoToCar;
import com.wong.certificacion.task.SearchProduct;



import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;



import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;


public class BuyProductStepDefinition {
	
	@Before
	    public void setTheStage() {
	        OnStage.setTheStage(new OnlineCast());
	        OnStage.theActorCalled("Mariana");
	    }
	

@Given("^that Mariana is in wong page$")
public void thatMarianaIsInWongPage() {
	 theActorInTheSpotlight().wasAbleTo(Open.url("https://www.wong.pe/especiales/cyberwong"));
	 theActorInTheSpotlight().attemptsTo(DeclinesNotifications.onPage());
	 
    
}

@When("^add a product to the shopping cart$")
public void addAProductToTheShoppingCart() {
	
	theActorInTheSpotlight().attemptsTo(BuyProduct.onPage());

}

@When("^searches the product (.*)$")
public void searchesTheProduct(String productName) {
	
	theActorInTheSpotlight().attemptsTo(SearchProduct.onPage(productName));
		
   
}

@Then("^validate the message: \"([^\"]*)\"$")
public void validateTheMessageProductoAgragadoAlCarrito(String message) {
	
    
	theActorInTheSpotlight().should(seeThat(ConfirmationMessageSuccessful.onPage(), Matchers.containsString(message)));
	theActorInTheSpotlight().attemptsTo(GoToCar.onPage());
	
}



@Then("^the product was successfully added to the cart$")
public void theProductWasSuccessfullyAddedToTheCart() {
	theActorInTheSpotlight()
	.should(seeThat(FindProductName.onCar(), Matchers.containsString(theActorInTheSpotlight().recall(PRODUCT_NAME_PAGE).toString())));
	
	
}

}
